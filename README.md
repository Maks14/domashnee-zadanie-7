<!-- Задание 1 -->

<!-- Инкапсуляция - это локализация т.е. разделение информации и данных на общую логику. 
Разделяют с помощью объявления классов, через "class название_класса". 
В классе объявляют или заносят свойства и методы, соответствующие по логике созданного класса. 
Данные созданные в классе, одни из них могут быть доступны и использованы - вне (за пределами этого класса), 
а другие доступны будут только внутри этого класса, а (за пределами этого класса использованы быть не могут). 
Объявляют такие свойства и методы через ключевые слова "public", "private" - . -->

<!-- Задание 2 -->

<!--  -->

<?php 
// Задание 3

class Car 
{
    public $model;
    public $price;
    public $bargain;
    
    public function __construct ($model, $price, $bargain)
    {
     $this->model = $model;
     $this->price = $price;  
     $this->bargain = $bargain;
    }

    // public function specifications ($object) {
    //     foreach ($object as $key => $value) {
    //         return  $key;
    //     }
    // }
}

class Television
{
    public $model;
    public $price;
    public $color;
    public $diagonal;

    public function __construct ($model, $price, $color, $diagonal)
    {
     $this->model = $model;
     $this->price = $price;  
     $this->color = $color;
     $this->diagonal = $diagonal;
    }
}

class BallPen
{
    public $price;
    public $color;
    public $kernel;
    

    public function __construct ($price, $color, $kernel)
    {
     $this->price = $price;  
     $this->color = $color;
     $this->kernel = $kernel;
    }
}

class Duck 
{
    public $breed;
    public $price;
    public $color;
    public $kernel;
    

    public function __construct ($breed, $price, $color, $kernel)
    {
     $this->breed = $breed;   
     $this->price = $price;  
     $this->color = $color;
     $this->kernel = $kernel;
    }
}

class Product 
{   
    public $title;
    public $description = [];
    public $price;

    public function __construct ($title, $price)
    {
     $this->title = $title;   
     $this->price = $price;  
    }
}

$audi = new Car('Ауди', '300$', 'Возможен торг');
$toyota = new Car('Ауди', '200$','Возможен торг');
$lg = new Television('LG', 150, 'black', 64);
$panasonic = new Television('Panasonic', 350, 'white', 80);
$pen1 = new BallPen(250, 'синяя', 'толстый');
$pen2 = new BallPen(150, 'черная', 'тонкий');
$broiler = new Duck('Бройлер', 3, 'белая', 1);
$blagovarskaya =  new Duck('Благоварская', 100, 'белая', 2);
$product1 = new Product('Название товара', 'Цена товара');
$product2 = new Product('Название товара', 'Цена товара');

var_dump($audi);
var_dump($toyota);
var_dump($lg);
var_dump($panasonic);
var_dump($pen1);
var_dump($pen2);
var_dump($broiler);
var_dump($blagovarskaya);
var_dump($product1);
var_dump($product2);

?>